List inToPost(List<String> list) {
  var operators = [];
  var postfix = [];
  for (var token in list) {
    if (int.tryParse(token) != null) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (operators.isNotEmpty &&
          operators.last != "(" &&
          precedence(token) <= precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(token);
    }
    if (token == '(') {
      operators.add(token);
    }
    if (token == ')') {
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }

  return postfix;
}

int precedence(String token) {
  switch (token) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
      return 2;
    case "(":
      return 3;
    default:
      return -1;
  }
}

bool isOperator(String token) {
  if (token == '+' || token == '-' || token == '*' || token == '/') {
    return true;
  }
  return false;
}

evaluatePost(List postfix) {
  var values = [];               
  for (var token in postfix) {
    if (int.tryParse(token) != null) {
      values.add(double.parse(token));
    } else {
      var r = values.removeLast();
      var l = values.removeLast();
      switch (token) {
        case '+':
          values.add(l + r);
          break;
        case '-':
          values.add(l - r);
          break;
        case '*':
          values.add(l * r);
          break;
        case '/':
          values.add(l / r);
          break;
      }
    }
  }
  return values;
}

void main(List<String> arguments) {
  var postfix = inToPost(arguments);
  print("Infix to postfix: $postfix");
  var evaluate = evaluatePost(postfix);
  print("Evaluate postfix: $evaluate");
}
