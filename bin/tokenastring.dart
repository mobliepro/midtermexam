void main(List<String> arguments) {
  var operators = [];
  var postfix = [];

  for (int i = 0; i < arguments.length; i++) {
    if (int.tryParse(arguments[i]) != null) {
      postfix.add(arguments[i]);
    }

    if (int.tryParse(arguments[i]) == null &&
        arguments[i] != '(' &&
        arguments[i] != ')') {

      while (operators.isNotEmpty &&
          operators.last != '(' &&
          (arguments[i] == '+' ||
              arguments[i] == '-' &&
                  operators.last != '*' &&
                  operators.last != '/' &&
                  arguments[i] == '*' ||
              arguments[i] == '/' && operators.last != ')')) {
        postfix.add(operators.removeLast());
      }
      operators.add(arguments[i]);
    }

    if (arguments[i] == '(') {
      operators.add(arguments[i]);
    }

    if (arguments[i] == ')') {
      
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }

  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }

  print(postfix);
}
