void main(List<String> arguments) {
  var values = [];

  for (int i = 0; i < arguments.length; i++) {
    if (int.tryParse(arguments[i]) != null) {
      values.add(int.parse(arguments[i]));
    } else {
      var right = values.removeLast();
      var left = values.removeLast();
      switch (arguments[i]) {
        case '+':
          var result = right + left;
          values.add(result);
          break;
        case '-':
          var result = right - left;
          values.add(result);
          break;
        case '*':
          var result = right * left;
          values.add(result);
          break;
        case '/':
          var result = left / right;
          values.add(result);
          break;
        default:
          break;
      }
    }
  }
  print(values.first);
}
